<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ "Log Book  ".ucwords(str_replace("-"," ",Request::segment(2))) }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
  <!-- Theme style -->
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-navbar-fixed">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
<!--       <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ asset('index3.html" class="nav-link">Home</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
<!--     <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar links -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link elevation-4">
      <img src="<?= asset('dist/img/AdminLTELogo.png') ?>"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-1 ">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="javascript:void(0)" class="align-items-center line-h-0 nav-link ">
                <i class="username-panel" aria-hidden="true">
                  <img src="http://pms.tridhyatech.com/img/profile.png" class="img-circle elevation-2" alt="User Image"></i>
                <p>
                  <span class="text-capitalize">
                   &nbsp {{ @auth::user()->first_name." ".@auth::user()->last_name }}
                  </span>
                <i class="right fas fa-angle-left" style="font-size: 23px;"></i></p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('editProfile') }}" class="nav-link">
                  <i class="nav-icon fas fa-user-circle"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('changePassword') }}" class="nav-link">
                  <i class="nav-icon fas fa-key"></i>
                  <p>Change Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= route('logout') ?>" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                 <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>Logout</p>
                  <form id="logout-form" action="<?= route('logout') ?>" method="POST" class="d-none">
                      @csrf
                  </form>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if(auth::user()->role == 0)
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('employee') }}" class="nav-link">
              <i class="nav-icon fas fa-users"></i>  
              <p>
                 Employees
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('superviser') }}" class="nav-link">
              <i class="nav-icon fas fa-user-shield"></i>
              <p>
                Supervisors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('departments') }}" class="nav-link">
              <i class="nav-icon fas fa-warehouse"></i>
              <p>
                Department
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('task') }}" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Task
              </p>
            </a>
          </li>
          @endif
          @if(auth::user()->role == 0 || auth::user()->role == 2)
          <li class="nav-item">
            <a href="{{ route('logsLisiting') }}" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Logs
              </p>
            </a>
          </li>
          @endif

          @if(auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ route('department') }}" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                Departments
              </p>
            </a>
          </li>

           <li class="nav-item">
            <a href="{{ route('myLogs') }}" class="nav-link">
              <i class="nav-icon fas fa-tasks"></i>
              <p>
                My Logs
              </p>
            </a>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer text-center">
    <strong>Copyright &copy; 2020-2021 <a href="#">Daily Log Book</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?= asset('plugins/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?= asset('plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?= asset('dist/js/adminlte.min.js') ?>"></script>

<!-- DataTables -->
<script src="<?= asset('plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') ?>"></script>
<script src="<?= asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') ?>"></script>
<script src="<?= asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<!-- AdminLTE App -->

<!-- AdminLTE for demo purposes -->
<script src="<?= asset('dist/js/demo.js') ?>"></script>
@if(session('MessageText'))
<script type="text/javascript">
$(document).ready(function(){
  toastr.{{ session("MessageType")}}("{{ session('MessageText') }}");
});
</script>
@endif
@yield('extraJS')
</body>
</html>
