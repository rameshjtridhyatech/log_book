@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row change_pass">
            <div class="col-md-6 m-auto" style="margin-top:50px!important;">                 
                <div class="card card-primary">
                    <div class="card-header bg-primary">
                        <h3 class="card-title"><b>Change Password</b></h3>
                    </div>
                    <form role="form" method="post" action="{{ route('savePassword') }}">
                    @csrf
                    <div class="card-body">
                            <div class="form-group">
                                <label for="password">
                                    Current Password
                                </label>
                                <input type="password" name="current_password" id="password" value="" autocomplete="new-password" class="form-control" placeholder="Current Password" value="{{ old('current_password')}}">
                                @error('current_password')
                                <p class="text-danger p-0 m-0">{{ $message }}</p>
                                @enderror
                            </div>
                            
                            <div class="form-group">
                                <label for="new_password" class="">
                                    New Password
                                </label>
                                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password" value="{{ old('new_password')}}">
                                @error('new_password')
                                <p class="text-danger p-0 m-0">{{ $message }}</p>
                                @enderror
                            </div>  
                            
                            <div class="form-group">
                                <label for="confirm_password" class="">
                                    Confirm password
                                </label>
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control" required="" aria-required="true" placeholder="Confirm Password" value="{{ old('confirm_password')}}">
                                @error('confirm_password')
                                <p class="text-danger p-0 m-0">{{ $message }}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <input id="submitBtn" type="submit" value="Save" class="btn btn-primary"> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
@endsection