@extends('layouts.app')
@section('content')
<br>
<br>
<div class="col-md-6 m-auto">
  <!-- general form elements -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title"><b>Editd Task</b></h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="{{ route('task.update') }}" method="post" enctype="multipart/form-data"> 
    @csrf
    <input type="hidden" name="id" value="{{ $task->id }}">
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control"  placeholder="Enter Name" name="name" value="{{ old('name',$task->name) }}">
        @error('name')
        <div class="help-block">
          <p class="text-danger">{{ $message }}</p>
        </div>
        @enderror
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<!-- /.card -->
</div>
@endsection