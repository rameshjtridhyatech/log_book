@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Task</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Task</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-10">
                    <h1 class="card-title mt-2"><b>Task Listing</b></h1>
                  </div>
                  <div class="col-md-2 ">
                    <a href="{{ route('task.create') }}" class="btn btn-sm btn-primary float-right">Add New</a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="task" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#Id</th>
                    <th>Name</th>
                    <th>Created At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @foreach($task as $row)
                    @php
                    $i++;
                    @endphp
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $row->name ?? 'N/A'}}</td>
                    <td>{{ date("Y-m-d",strtotime($row->created_at)) ?? 'N/A'}}</td>
                    <td><a href="{{ route('task.edit',$row->id) }}" class="btn btn-sm btn-warning">Edit</a> <a href="{{ route('task.delete',$row->id) }}" class="btn btn-sm btn-danger">Delete</a></td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section("extraJS")
<script>
  $(function () {
    $("#task").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
