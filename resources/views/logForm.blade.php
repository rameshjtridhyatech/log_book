@extends('layouts.app')
@section('content')
<br>
<br>
<div class="col-md-6 m-auto">
  <!-- general form elements -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title"><b>Add My Log Entry</b></h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="{{ route('saveLog') }}" method="post" enctype="multipart/form-data"> 
    @csrf
    <input type="hidden" name="superviser_id" value="{{ $user->id }}">
    <input type="hidden" name="department_id" value="{{ $department->id }}">

    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Superviser</label>
        <input type="text" class="form-control"   name="superviser" value="{{ $user->first_name.' '.$user->last_name }}" readonly="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Department</label>
        <input type="text" class="form-control"   name="department" value="{{ $department->name }}" readonly="">
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Date</label>
        <input type="date" class="form-control"   name="date" value="{{ date('Y-m-d') }}" required="" >
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Task</label>
        <select class="form-control" name="task" required="">
          <option value="-1">Other Task</option>
          @if(!empty($task))
          @foreach($task as $row)
          <option value="{{ $row->id }}">{{ $row->name}}</option>
          @endforeach
          @endif
        </select>
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Other Task</label>
        <input type="text" name="othertask" value="" class="form-control" >
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Houres</label>
        <input type="number" name="houres" value="" class="form-control" required="">
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<!-- /.card -->
</div>
@endsection