@extends('layouts.app')
@section('content')
<br>
<br>
<div class="col-md-10 m-auto">
  <!-- general form elements -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title"><b> Profile</b></h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="{{ route('saveProfile') }}" method="post" enctype="multipart/form-data"> 
    @csrf
    <div class="card-body">
      <div class="row">
        <input type="hidden" name="role" value="{{ $user->role}}">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">First Name</label>
            <input type="text" class="form-control"  placeholder="Enter First Name" name="first_name" value="{{ old('first_name',$user->first_name) }}">
            @error('first_name')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Last Name</label>
            <input type="text" class="form-control"  placeholder="Enter Last Name" name="last_name" value="{{ old('last_name',$user->last_name) }}">
            @error('last_name')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Images</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="image">
                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
              </div>
            </div>
              @error('image')
              <div class="help-block">
                  <p class="text-danger">{{ $message }}</p>
              </div>
              @enderror
              <img src="{{ asset('images/').'/'.$user->image }}"  style="height: 100px;margin-top: 10px;">
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control"  placeholder="EMail" name="email" value="{{ old('email',$user->email) }}" readonly="">
            @error('email')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Contact</label>
            <input type="text" class="form-control"  placeholder="Contact" name="contact" value="{{ old('contact',$user->contact) }}">
            @error('contact')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            <textarea class="form-control"  placeholder="Enter Address" name="address">{{ old('address',$user->address) }}</textarea>
            @error('address')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>
        </div>
      
      </div>

      @if($user->role == 2)
      <div class="row">
        <div class="col-md-12" style="border-top: 3px dashed  blue; margin-bottom: 10px;"></div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Birthdate</label>
            <input type="date" class="form-control"   name="birthdate" value="{{ old('birthdate',$user->birthdate) }}">
            @error('birthdate')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Exprerience</label>
            <input type="text" class="form-control"  placeholder="Exprerience" name="exprerience" value="{{ old('exprerience',$user->experience) }}">
            @error('exprerience')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Department</label>
            <select class="form-control" name="department">
              <option value="">Select Department</option>
              @if(!empty($department))
              @foreach($department as $row)
              <option value="{{ $row->id}}" {{ $user->departments_id == $row->id ? 'selected' : '' }}>{{ $row->name ?? ''}}</option>
              @endforeach
              @endif
            </select>
            @error('department')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>
        </div>
        
        <div class="col-md-6">

          <div class="form-group">
            <label for="exampleInputEmail1">Document Type</label>
            <select class="form-control" name="document_type">
              <option value="">Select Document</option>
              <option value="Addharcard" {{ $user->id_proff == "Addharcard" ? "selected" : ""}}>Addharcard</option>
              <option value="PAN Card" {{ $user->id_proff == "PAN Card" ? "selected" : ""}}>PAN Card</option>
              <option value="Driving Licence" {{ $user->id_proff == "Driving Licence" ? "selected" : ""}}>Driving Licence</option>
            </select>
            @error('document_type')
            <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
          </div>

          <div class="form-group">
            <label for="exampleInputFile">Document</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" class="custom-file-input" name="document">
                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
              </div>
            </div>
              @error('document')
              <div class="help-block">
                  <p class="text-danger">{{ $message }}</p>
              </div>
              @enderror
              <img src="{{ asset('images/').'/'.$user->id_image }}"  style="height: 100px;margin-top: 10px;">
          </div>
        </div>
      </div>
      @endif
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<!-- /.card -->
</div>
@endsection