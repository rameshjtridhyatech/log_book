@extends('auth.authMaster')

@section('title','Registration')
@section('content')
<div class="row">
	<div class="col-md-5">
		<img src="{{ asset('img/Registration-Banner1.png') }}" width="100%" style="margin-top: 50px;">
	</div>
	<div class="col-md-7">
		<div class="register-box m-auto" style="width: 430px;">
		  <div class="card">
		    <div class="card-body register-card-body">
		      <p class="login-box-msg">Register a new membership</p>

		      <form action="{{ route('saveUser') }}" method="post" enctype="multipart/form-data">
		      	@csrf
		      	@error('first_name')
			    <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		          <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{ old('first_name') }}">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-user"></span>
		            </div>
		          </div>
		        </div>

		         @error('last_name')
			              <p class="text-danger p-0 m-0">{{ $message }}</p>
			      @enderror
		        <div class="input-group mb-3">
		          <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-user"></span>
		            </div>
		          </div>
		        </div>

		        @error('email')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		          <input type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" name="email">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-envelope"></span>
		            </div>
		          </div>
		        </div>
		        @error('password')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		          <input type="password" class="form-control" placeholder="Password" name="password" >
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <span class="fas fa-lock"></span>
		            </div>
		          </div>
		        </div>

		        @error('image')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		          	<div class="custom-file">
		            	<input type="file" class="custom-file-input" name="image">
		            	<label class="custom-file-label" for="exampleInputFile">Choose file</label>
		            </div>
		        </div>

		        @error('contact')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		          <input type="text" class="form-control" placeholder="Contact No" name="contact" value="{{ old('contact') }}">
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <i class="fas fa-address-book"></i>
		            </div>
		          </div>
		        </div>

		        @error('address')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		        	<textarea name="address" class="form-control" placeholder="Enter Address">{{ old('address') }}</textarea>
		          <div class="input-group-append">
		            <div class="input-group-text">
		              <i class="fas fa-map-marked-alt"></i>
		            </div>
		          </div>
		        </div>
		        @error('user_type')
			        <p class="text-danger p-0 m-0">{{ $message }}</p>
			    @enderror
		        <div class="input-group mb-3">
		        	<select name="user_type" class="form-control" style="border-right: 1px solid lightgray;">
		        		<option value="1">Employee </option>
		        		<option value="2">Superviser</option>
		        	</select>
		        </div>
		          <!-- /.col -->
		            <div class="row">
			          <div class="col-4">
			            <button type="submit" class="btn btn-primary btn-block">Register</button>
			          </div>
			          <div class="col-8">
						      <a href="{{ route('login') }}" class="text-right float-right mt-3">I already have a acount.</a>
			          </div>
		      		</div>
		          <!-- /.col -->
		        </div>
		      </form>
		    </div>
		    <!-- /.form-box -->
		  </div><!-- /.card -->
		</div>	
	</div>
</div>
@endsection