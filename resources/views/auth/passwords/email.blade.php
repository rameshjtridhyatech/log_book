@extends('auth.authMaster')

@section('title','Forget Password')
@section('content')
<div class="login-box">
  <!-- /.login-logo -->
    @if(session('status'))
      <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><i class="icon fas fa-check"></i> Success!</h5>
          {{ session('status') }}
      </div>
    @endif
  <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Reset Password</h3>
      </div>
    <div class="card-body login-card-body">
      <p class="login-box-msg">Enter email to get reset password link in your E-mail.</p>
      
      <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="input-group mb-3">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail Address">
          <div class="input-group-append" >
            <div class="input-group-text" style="border-radius: 0px 5px 5px 0px!important;">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">Send Password Reset Link</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
      <p class="mb-1">
        <a href="{{ route('login') }}">Back to Login.</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
@endsection
