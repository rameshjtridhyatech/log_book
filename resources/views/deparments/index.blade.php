@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Departments</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Department</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-10">
                    <h1 class="card-title mt-2"><b>Departments Listing</b></h1>
                  </div>
                  <div class="col-md-2 ">
                    <a href="{{ route('departments.create') }}" class="btn btn-sm btn-primary float-right">Add New</a>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="department" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#Id</th>
                    <th>Name</th>
                    <th>Images</th>
                    <th>Description</th>
                    <th>Created At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @foreach($deparments as $row)
                    @php
                    $i++;
                    @endphp
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $row->name ?? 'N/A'}}</td>
                    <td><img src="{{ asset('images/').'/'.$row->image }}" style="width: 100px;"></td>
                    <td>{{ $row->description ?? 'N/A'}}</td>
                    <td>{{ date("Y-m-d",strtotime($row->created_at)) ?? 'N/A'}}</td>
                    <td><a href="{{ route('departments.edit',$row->id) }}" class="btn btn-sm btn-warning">Edit</a> <a href="{{ route('departments.delete',$row->id) }}" class="btn btn-sm btn-danger">Delete</a></td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section("extraJS")
<script>
  $(function () {
    $("#department").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
