@extends('layouts.app')
@section('content')
<br>
<br>
<div class="col-md-6 m-auto">
  <!-- general form elements -->
<div class="card card-primary">
  <div class="card-header">
    <h3 class="card-title"><b>Edit Department</b></h3>
  </div>
  <!-- /.card-header -->
  <!-- form start -->
  <form role="form" action="{{ route('departments.update') }}" method="post" enctype="multipart/form-data"> 
    @csrf
    <input type="hidden" name="id" value="{{ @$deparment->id }}">
    <div class="card-body">
      <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control"  placeholder="Enter Name" name="name" value="{{ old('name',@$deparment->name) }}">
        @error('name')
        <div class="help-block">
          <p class="text-danger">{{ $message }}</p>
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Description</label>
        <textarea class="form-control" name="description" placeholder="Enter Description">{{ old('name',@$deparment->description) }}</textarea>
        @error('description')
        <div class="help-block">
          <p class="text-danger">{{ $message }}</p>
        </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputFile">Images</label>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" class="custom-file-input" name="image">
            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
          </div>
        </div>
          @error('image')
          <div class="help-block">
              <p class="text-danger">{{ $message }}</p>
          </div>
          @enderror
          <img src="{{ asset('images/').'/'.$deparment->image }}" style="width: 100px; margin-top: 10px;">
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<!-- /.card -->
</div>
@endsection