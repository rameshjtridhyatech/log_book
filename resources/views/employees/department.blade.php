@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Department</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Department</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header bg-primary">
                    <h1 class="card-title"><b>Departments</b></h1>
                </div>
              <!-- /.card-header -->
              <div class="card-body row">
                  @foreach($department as $row)
                    @php
                    $user = \App\User::where("departments_id",$row->id)->first();
                    @endphp
                    <div class="col-md-4">
                      <div class="card">
                        <img class="card-img-top" src="{{ asset('images/').'/'.$row->image }}" alt="Card image" style="width:100%">
                        <div class="card-body">
                          <h4 class="card-title pb-1 w-100"><b>Department</b> : {{ $row->name }}</h4><br>
                          <h4 class="card-title pb-1 w-100"><b>SuperViser</b> : {{ @$user->first_name." ".@$user->last_name }}</h4>
                          <h4 class="card-title pb-1 w-100"><b>Email</b> : {{ @$user->email }}</h4>
                          <h4 class="card-title pb-1 w-100"><b>Contact </b> : {{ @$user->contact }}</h4>
                          <h4 class="card-title pb-1 w-100"><b>Address</b> : {{ @$user->address }}</h4>
                          <p class="card-text">{{ $row->description ?? 'N/A'}}</p>
                          @if(!empty($user))
                          <a href="{{ route('addLog',[$user->id,$row->id]) }}" class="btn btn-primary">Log Entry</a>
                          @endif
                        </div>
                      </div>
                    </div>
                  @endforeach
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section("extraJS")
<script>
</script>
@endsection
