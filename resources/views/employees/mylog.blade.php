@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1>My Logs</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">My Logs</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                    <h1 class="card-title mt-2"><b>Daily Log Listing</b></h1>
                </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="DataTable" class="table table-bordered table-striped text-capitalize">
                  <thead>
                  <tr>
                    <th>#Id</th>
                    <th>Department</th>
                    <th>Superviser</th>
                    <th>Task</th>
                    <th>Other Task</th>
                    <th>Houres</th>
                    <th>Date</th>
                    <th>status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @foreach($logs as $row)
                    @php
                    $i++;
                    @endphp
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $row->department->name ?? 'N/A'}}</td>
                    <td class="text-lowercase">{{ $row->user->first_name." ".$row->user->last_name ?? 'N/A'}}</td>
                    <td>{{ $row->task->name ?? 'N/A'}}</td>
                    <td>{{ $row->other_task ?? 'N/A'}}</td>
                    <td>{{ $row->houres ?? 'N/A'}}</td>
                    <td>{{ $row->date ?? 'N/A'}}</td>
                    <td>{{ $row->status == 0 ? "Not Appoved" : "Approved"}}</td>
                    <td>
                      @if($row->status == 0 )
                      <a href="{{ route('logs.delete',$row->id) }}" class="btn btn-sm btn-danger">Delete</a>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section("extraJS")
<script>
  $(function () {
    $("#DataTable").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
