@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6">
            <h1>EMployees</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employees</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                    <h1 class="card-title mt-2"><b>Employee Listing</b></h1>
                </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="DataTable" class="table table-bordered table-striped text-capitalize">
                  <thead>
                  <tr>
                    <th>#Id</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Images</th>
                    <th>Contact</th>
                    <th>Addreess</th>
                    <th>Created At</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php
                    $i = 0;
                    @endphp
                    @foreach($employee as $row)
                    @php
                    $i++;
                    @endphp
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $row->first_name." ".$row->last_name ?? 'N/A'}}</td>
                    <td class="text-lowercase">{{ $row->email ?? 'N/A'}}</td>
                    <td><img src="{{ asset('images/').'/'.$row->image }}" style="width: 100px;"></td>
                    <td class="text-lowercase">{{ $row->contact ?? 'N/A'}}</td>
                    <td class="text-lowercase">{{ $row->address ?? 'N/A'}}</td>
                    <td>{{ date("Y-m-d",strtotime($row->created_at)) ?? 'N/A'}}</td>
                    <td>
                      @if($row->status == 1)
                      <a href="{{ route('users.status',$row->id) }}" class="btn btn-sm btn-success">Approve</a>
                      @else
                      <a href="{{ route('users.status',$row->id) }}" class="btn btn-sm btn-primary">Not Approve</a>
                      @endif
                      <a href="{{ route('users.delete',$row->id) }}" class="btn btn-sm btn-danger">Delete</a>
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
@endsection

@section("extraJS")
<script>
  $(function () {
    $("#DataTable").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
@endsection
