<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Session;

class DepartmentControllers extends Controller
{
    public function index(Request $request)
    {
        $deparments = Department::all();
    	return view("deparments.index",compact('deparments'));
    }

    public function create()
    {
     return view("deparments.create");   
    }

    public function store(Request $request)
    {
		$validatedData = $request->validate([
           'name' => 'required|max:100',
           'description' => 'required|max:500',
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2014',
        ]);

        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);

        $Department = new Department;
        $Department->name = $request->name;
        $Department->description = $request->description;
        $Department->image = $imageName;
        $Department->save();

        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Department added successfully.'); 
        return redirect()->route('departments');
    }

    public function edit(Request $request,$id)
    {
        $deparment = Department::find($id);
        return view("deparments.edit",compact('deparment'));
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
           'id' => 'required',
           'name' => 'required|max:100',
           'description' => 'required|max:500',
           'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2014',
        ]);

        if(isset($request->image)){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        }

        $Department = Department::find($request->id);
        $Department->name = $request->name;
        $Department->description = $request->description;
        if(isset($request->image)){
            $Department->image = $imageName;
        }        
        $Department->save();

        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Department updated successfully.'); 
        return redirect()->route('departments');
    }

    public function delete($id)
    {
        $delete = Department::where("id",$id)->delete();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Department deleted successfully.'); 
        return redirect()->route('departments');
    }	

    public function lisitng()
    {
        $department = Department::all();
        return view("employees.department",compact('department'));        
    }
}
