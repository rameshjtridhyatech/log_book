<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LogsBook;
use App\Department;
use App\User;
use App\Task;
use Auth;
use Session;

class LogBookController extends Controller
{
    public function addLog(Request $request,$sup,$dept)
    {
    	$user = User::find($sup);
    	$department = Department::find($dept);
    	$task = Task::all();
    	return view("logForm",compact('user','department','task'));
    }


    public function saveLog(Request $request)
    {	
    	$LogsBook = LogsBook::where("emp_id",Auth::User()->id)->whereDate("date","=",$request->date)->first();
    	$LogsBook = LogsBook::where("emp_id",Auth::User()->id)->whereDate("date","=",$request->date)->first();
    	if(empty($LogsBook)){
    		$LogsBook = new LogsBook;
    	}
    	$LogsBook->emp_id = Auth::User()->id;
    	$LogsBook->superviser_id = $request->superviser_id;
    	$LogsBook->department_id = $request->department_id;
    	$LogsBook->task_id = $request->task;
    	$LogsBook->date = $request->date;
    	$LogsBook->other_task = $request->othertask;
    	$LogsBook->houres = $request->houres;
    	$LogsBook->save();

    	Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Log entry added successfully.'); 
        return redirect()->route('myLogs');	
    }

    public function myLogs()
    {
    	$logs = LogsBook::where("emp_id",Auth::User()->id)->get();
    	return view("employees.mylog",compact('logs'));
    }

    public function delete(Request $request,$id)
    {
    	$delete  = LogsBook::where("id",$id)->delete();
    	Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Log entry deleted successfully.'); 
        return back();
    }

    public function Logs(Request $request)
    {	
    	if(Auth::User()->role == 2 ){
	    	if(!empty(Auth::User()->email_verified_at) && Auth::User()->status != 0){
		    	$logs = LogsBook::where("superviser_id",Auth::User()->id)->get();    		
	    	}else{
		    	Session::flash('MessageType', 'error'); 
		        Session::flash('MessageText', 'Please First Complet Your Profile and Get Approve From Admin.'); 
	    		return redirect()->route("home");
	    	}
    	}else{
    	$logs = LogsBook::all();
    	}
    	return view("logs",compact('logs'));
    }

    public function changeStatus(Request $request,$id)
    {
    	$LogsBook = LogsBook::find($id);
    	$LogsBook->status = $LogsBook->status == 1 ? 0 : 1; 
    	$LogsBook->save(); 	
		
		Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Log status changed successfully.'); 
        return back();
    }
}
