<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Session;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        $task = Task::all();
    	return view("task.index",compact('task'));
    }

    public function create()
    {
     return view("task.create");   
    }

    public function store(Request $request)
    {
		$validatedData = $request->validate([
           'name' => 'required|max:100'
        ]);


        $Task = new Task;
        $Task->name = $request->name;
        $Task->save();

        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Task added successfully.'); 
        return redirect()->route('task');
    }

    public function edit(Request $request,$id)
    {
        $task = Task::find($id);
        return view("task.edit",compact('task'));
    }

    public function update(Request $request)
    {
        $validatedData = $request->validate([
           'id' => 'required',
           'name' => 'required|max:100'
        ]);

        $Task = Task::find($request->id);
        $Task->name = $request->name;
        $Task->save();

        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Task updated successfully.'); 
        return redirect()->route('task');
    }

    public function delete($id)
    {
        $delete = Task::where("id",$id)->delete();
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Task deleted successfully.'); 
        return redirect()->route('task');
    }	
}
