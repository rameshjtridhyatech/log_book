<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use App\Department;

class UserControllers extends Controller
{
    public function register(Request $request)
    {
    	return view("registration");
    }

    public function save(Request $request)
    {
    	$validatedData = $request->validate([
           'first_name' => 'required|max:100',
           'last_name' => 'required|max:100',
           'email' => 'required|email|unique:users,email',
           'password' => 'required|min:6',
           'contact' => 'required|max:100',
           'address' => 'required|max:100',
           'user_type' => 'required',
           'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2014',
        ]);


        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('images'), $imageName);

    	$user = new User;
    	$user->first_name = $request->first_name;
    	$user->last_name = $request->last_name;
    	$user->email = $request->email;
    	$user->contact = $request->contact;
    	$user->image   = $imageName;
    	$user->address = $request->address;
    	$user->role = $request->user_type;
    	$user->password = \Hash::make($request->password);
    	$user->save();

    	Session::flash('MessageType', 'success'); 
      Session::flash('MessageText', 'User reguster successfully.'); 
      return redirect()->route('login');
    }

    public function employeeListing()
    {
      $employee = User::where("role",1)->get();
      return view("employees.employee_listing",compact('employee'));
    }

    public function superviserLisitng()
    {
      $superviser = User::where("role",2)->get();    	
      return view("superviser.superviser_listing",compact('superviser'));
    }

    public function changeStatus(Request $request,$id)
    {
      $user = User::find($id);
      $user->status = $user->status == 1 ? 0 : 1; 
      $user->save(); 

      Session::flash('MessageType', 'success'); 
      Session::flash('MessageText', 'Status changed successfully.'); 
      return back();
    }

    public function delete(Request $request,$id)
    {
      $user = User::where("id",$id)->delete();
      Session::flash('MessageType', 'success'); 
      Session::flash('MessageText', 'User reguster successfully.'); 
      return back();
    }

    public function savePassword(Request $request)
    {
      $validatedData = $request->validate([
           'current_password' => 'required|min:6',
           'new_password' => 'required|min:6',
           'confirm_password' => 'required_with:new_password|same:new_password|min:6'
        ]);

      $current_password = Auth::User()->password;           
      if(\Hash::check($request->current_password, $current_password))
      {           
        $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);
        $obj_user->password = \Hash::make($request->new_password);
        $obj_user->save(); 
        Session::flash('MessageType', 'success'); 
        Session::flash('MessageText', 'Password Reset successfully.'); 
        return redirect()->route('home');  
      }
      else
      {           
        Session::flash('MessageType', 'error'); 
        Session::flash('MessageText', 'Please enter correct current password !'); 
        return back();  
      }
    }

    public function changePassword(Request $request)
    {
      return view("changePassword");
    }

    public function editProfile()
    {
      $user = Auth::User();
      $department = Department::all();
      return view("view_profile",compact('user','department'));
    }

    public function saveProfile(Request $request)
    {
        $validatedData = $request->validate([
           'first_name' => 'required|max:100',
           'last_name' => 'required|max:100',
           'email' => 'required|email|unique:users,email,'.Auth::User()->id,
           'contact' => 'required|max:100',
           'address' => 'required|max:100',
           'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2014',
           'birthdate' => 'required_if:role,2',
           'exprerience' => 'required_if:role,2',
           'department' => 'required_if:role,2',
           'document_type' => 'required_if:role,2',
           'document' => 'required_if:role,2|image|mimes:jpeg,png,jpg,gif,svg|max:2014',
        ],[
          'required_if' => 'The field is required.'
        ]);

        if(isset($request->image)){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
        }

        if(isset($request->document)){
            $documentName = time().'.'.request()->document->getClientOriginalExtension();
            request()->document->move(public_path('images'), $documentName);
        }

      $user = User::find(Auth::User()->id);
      $user->first_name = $request->first_name;
      $user->last_name = $request->last_name;
      $user->email = $request->email;
      $user->contact = $request->contact;
      $user->address = $request->address;      
      if(isset($request->image)){
        $user->image   = $imageName;
      }

      if(isset($request->document)){
        $user->id_image = $documentName;
      }
      if(isset($request->birthdate)){
        $user->birthdate = $request->birthdate;
      }
      if(isset($request->department)){
        $user->departments_id = $request->department;
      }
      if(isset($request->exprerience)){
        $user->experience = $request->exprerience;
      }
      if(isset($request->document_type)){
        $user->id_proff = $request->document_type;
      }
      $user->email_verified_at = date("Y-m-d H:i:s");
      $user->save();

      Session::flash('MessageType', 'success'); 
      Session::flash('MessageText', 'Profile updated successfully.'); 
      return back(); 
    }
}
