<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $totalEmployee = User::where("role",1)->count();
        $totalSuperviser = User::where("role",2)->count();
        $totalDepartment = Department::count();
        if(Auth::User()->role == 1){
            return redirect()->route('department');
         }else if(Auth::User()->role == 2){  
            return redirect()->route('editProfile');
        }else{
            return view('home',compact('totalEmployee','totalSuperviser','totalDepartment'));
        }
    }
}
