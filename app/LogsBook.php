<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogsBook extends Model
{
    protected $table = "logs_books_tables";

    public function department()
    {
        return $this->belongsTo('App\Department','department_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','superviser_id');
    }

    public function emp()
    {
        return $this->belongsTo('App\User','emp_id');
    }

    public function task()
    {
        return $this->belongsTo('App\Task','task_id');
    }
}
