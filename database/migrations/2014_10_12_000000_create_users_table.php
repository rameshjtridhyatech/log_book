<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string("last_name")->nullable();
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string("image")->nullable();
            $table->string("contact")->nullable();
            $table->string("address")->nullable();
            $table->integer("role")->default(0)->comment("0 = Admin , 1 => Employee , 2 Supervizer");
            $table->integer("status")->default(0)->comment("0 = Not Approve , 1 => Approve");
            $table->string("birthdate")->nullable();
            $table->integer("departments_id")->nullable();
            $table->string("experience")->nullable();
            $table->string("id_proff")->nullable();
            $table->string("id_image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
