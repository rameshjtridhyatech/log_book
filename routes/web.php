<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/registrationUser', 'UserControllers@register')->name('registrationUser');
Route::post('/saveUser', 'UserControllers@save')->name('saveUser');

Route::middleware('auth')->group(function()
{
	Route::get('/changePassword', 'UserControllers@changePassword')->name('changePassword');
	Route::post('/savePassword', 'UserControllers@savePassword')->name('savePassword');
	Route::get('/editProfile', 'UserControllers@editProfile')->name('editProfile');
	Route::post('/saveProfile', 'UserControllers@saveProfile')->name('saveProfile');
	Route::get('/logsLisiting', 'LogBookController@index')->name('logsLisiting');
	Route::get('/department', 'DepartmentControllers@lisitng')->name('department');
	Route::get('/addLog/{sup}/{dept}', 'LogBookController@addLog')->name('addLog');
	Route::post('/saveLog', 'LogBookController@saveLog')->name('saveLog');
	Route::get('/myLogs', 'LogBookController@myLogs')->name('myLogs');
	Route::get('/logs/delete/{id}', 'LogBookController@delete')->name('logs.delete');
	Route::get('/Logs', 'LogBookController@Logs')->name('logsLisiting');
	Route::get('/Logs/status/{id}', 'LogBookController@changeStatus')->name('change.logStatus');
});

Route::prefix("admin")->middleware('auth')->group(function()
{
	Route::prefix("departments")->group(function()
	{
		Route::get('/', 'DepartmentControllers@index')->name('departments');
		Route::get('/create', 'DepartmentControllers@create')->name('departments.create');
		Route::post('/store', 'DepartmentControllers@store')->name('departments.store');
		Route::get('/edit/{id}', 'DepartmentControllers@edit')->name('departments.edit');
		Route::post('/update', 'DepartmentControllers@update')->name('departments.update');
		Route::get('/delete/{id}', 'DepartmentControllers@delete')->name('departments.delete');
	});

	Route::prefix("task")->group(function()
	{
		Route::get('/', 'TaskController@index')->name('task');
		Route::get('/create', 'TaskController@create')->name('task.create');
		Route::post('/store', 'TaskController@store')->name('task.store');
		Route::get('/edit/{id}', 'TaskController@edit')->name('task.edit');
		Route::post('/update', 'TaskController@update')->name('task.update');
		Route::get('/delete/{id}', 'TaskController@delete')->name('task.delete');
	});

	Route::prefix("users")->group(function()
	{
		Route::get('/employee', 'UserControllers@employeeListing')->name('employee');
		Route::get('/superviser', 'UserControllers@superviserLisitng')->name('superviser');
		Route::get('/delete/{id}', 'UserControllers@delete')->name('users.delete');
		Route::get('/status/{id}', 'UserControllers@changeStatus')->name('users.status');
	});
});